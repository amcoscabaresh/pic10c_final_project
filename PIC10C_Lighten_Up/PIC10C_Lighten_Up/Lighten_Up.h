#include <string>
#include <vector>
#include <iostream>
#include <array>

#ifndef LIGHTEN_UP_H
#define LIGHTEN_UP_H

using namespace std;

// A box object will have a top, middle, and bottom array of chars that can be outputed by our draw_grid function
// A box objects also includes a special val which will show if the box is filled, contains a clue, or has a light placed in it
class Box {

public:
	
	// Default constructor
	Box();

	// accessor functions
	char* get_top();
	char* get_middle();
	char* get_bottom();
	int get_tap_count();
	char get_val();
	int get_light_count();

	// Basic functions to change variables
	void increase_tap_count();
	void increase_light_count();
	void decrease_light_count();
	// allows us to change val when player makes a move
	void change_val(char c);

private:
	int tap_count;
	int light_count;
	char val;
	char top[5] = { char(218), char(196), char(196), char(196), char(191) };
	char middle[5] = { char(179), char(255), char(255), char(255), char(179) };
	char bottom[5] = { char(192), char(196), char(196), char(196), char(217) };
};










#endif