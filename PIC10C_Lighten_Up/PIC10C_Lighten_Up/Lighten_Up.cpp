#include "Lighten_Up.h"
using namespace std;

// Draw a grid of individual boxes
void draw_grid(Box grid[5][5]);

// Create various game boards
void create_board1(Box grid[5][5]);
void create_board2(Box grid[5][5]);
void create_board3(Box grid[5][5]);
void create_board4(Box grid[5][5]);
void create_board5(Box grid[5][5]);

// Print out welcome message and instructions
int welcome();

// Continue allowing player to tap squares until they decide to submit their answer
void play_game(Box grid[5][5]);

// The first time a box is tapped, a lightbulb appears, illuminating the squares above, below, and to the side of it
// The second time a box is tapped, the lightbulb is removed and the squares it was lighting become blank again
void tap(Box grid[5][5], int r, int c);

// Tests the provided grid against the rules of the game to see if the solution is valid
void check_answer(Box grid[5][5]);

int main() {

	int board = welcome();
	Box grid[5][5];
	
	// Fills grid using default constructor for all boxes
	for (int i = 0; i <= 4; ++i) {
		for (int j = 0; j <= 4; ++j) {
			grid[i][j] = Box();
		}
	}
	
	if (board == 1) {
		create_board1(grid);
	}
	if (board == 2) {
		create_board2(grid);
	}
	if (board == 3) {
		create_board3(grid);
	}
	if (board == 4) {
		create_board4(grid);
	}
	if (board == 5) {
		create_board5(grid);
	}
	
	draw_grid(grid);
	play_game(grid);
	check_answer(grid);

	return 0;
}

// Now draws entire grid using 2d array
void draw_grid(Box grid[5][5]) {
	// Outputs five rows of five boxes
	for (int i = 0; i < 5; ++i) {
		// Outputs top of the row of boxes
		for (int j = 0; j < 5; ++j) {
			for (int k = 0; k < 5; ++k) {
				cout << grid[i][j].get_top()[k];
			}
		}
		cout << endl;
		// Outputs middle of the row of boxes
		for (int j = 0; j < 5; ++j) {
			for (int k = 0; k < 5; ++k) {
				cout << grid[i][j].get_middle()[k];
			}
		}
		cout << endl;
		// Outputs bottom of the row of boxes
		for (int j = 0; j < 5; ++j) {
			for (int k = 0; k < 5; ++k) {
				cout << grid[i][j].get_bottom()[k];
			}
		}
		cout << endl;
	}
	cout << endl;
	return;
}

// Creates first board option
void create_board1(Box grid[5][5]) {
	grid[0][2].change_val('#');
	grid[0][4].change_val('#');
	grid[1][2].change_val('2');
	grid[2][0].change_val('1');
	grid[4][3].change_val('2');
	return;
}

// Creates second board option
void create_board2(Box grid[5][5]) {
	grid[2][0].change_val('0');
	grid[2][1].change_val('0');
	grid[4][1].change_val('1');
	grid[4][3].change_val('1');
	grid[2][4].change_val('1');
}

// Creates third board option
void create_board3(Box grid[5][5]) {
	grid[0][1].change_val('1');
	grid[3][0].change_val('1');
	grid[2][3].change_val('0');
	grid[4][2].change_val('#');
	grid[4][3].change_val('1');
}

// Creates fourth board option
void create_board4(Box grid[5][5]) {
	grid[1][1].change_val('2');
	grid[0][3].change_val('1');
	grid[1][3].change_val('#');
	grid[2][3].change_val('0');
	grid[3][4].change_val('1');
}

// Creates fifth board option
void create_board5(Box grid[5][5]) {
	grid[4][0].change_val('0');
	grid[0][3].change_val('0');
	grid[0][4].change_val('0');
	grid[1][3].change_val('1');
	grid[3][3].change_val('1');
}

// Outputs welcome message and instructions
int welcome() {
	int board;
	cout << "Welcome to Lighten Up!" << endl;
	cout << "The grid represents a room where squares marked with a #,0,1,2,3,4 are walls." << endl;
	cout << "Your goal is to light up the entire room following these rules:" << endl;
	cout << "1. Lightbulbs lite all unblocked squares horizontaly and vertically" << endl;
	cout << "2. A lightbult cannot light another lightbulb" << endl;
	cout << "3. Walls block light" << endl;
	cout << "4. Walls with a number tell you how many lightbulbs are horizontally and vertically adjacent to it." << endl;
	cout << "A wall with a # can have any number of lightbulbs adjacent to it." << endl;
	cout << "If you \"tap\" a box once, a lightbulb will be placed there. If you \"tap\" it again, the lighbulb will be removed" << endl << endl;
	// User selects which board they would like to use
	cout << "You can choose from 5 boards! Please enter a number between 1 and 5 to choose your board: ";
	cin >> board;
	return board;
}

// Until the player enters "5 5" the game continues by prompting the player to tap a box
void play_game(Box grid[5][5]) {
	int row;
	int column;
	bool done = false;
	// Instructions of how to enter coordinates
	cout << "To tap a box, enter two numbers separated by a space to indicate the coordiates of your chosen square. (The coordinates operate like those of a matrix, starting at 0)" << endl;
	while (!done) {
		// Prompts user to pick a box
		cout << "Which box would you like to tap? (Enter \"5 5\" to submit your final answer)" << endl;
		cin >> row >> column;
		// Registers if the user would like to submit their answer
		if (row == 5 && column == 5) {
			done = true;
			continue;
		}
		// Verrify that the coordinates entered are valid
		if (row < 0 || row > 4 || column < 0 || column > 4) {
			cout << "Coordinates must be between 0 and 4! ";
			continue;
		}
		// Tap the requested box and redraw the grid
		tap(grid, row, column);
		draw_grid(grid);
		cout << endl;
	}
	
}

// The first time a box is tapped, a lightbulb appears, illuminating the squares above, below, and to the side of it
// The second time a box is tapped, the lightbulb is removed and the squares it was lighting become blank again
void tap(Box grid[5][5], int r, int c) {
	char val = grid[r][c].get_val();
	// Verrify that the user is taping a wall
	if (val != ' ' && val != '*' && val != '~') {
		cout << "Cannot place a lightbulb on a wall!" << endl;
		return;
	}
	grid[r][c].increase_tap_count();
	int tap_count = grid[r][c].get_tap_count();
	if (tap_count % 2 == 0) {
		// Removes lightbulb
		if (grid[r][c].get_light_count() == 0) {
			grid[r][c].change_val(' ');
		}
		else {
			grid[r][c].change_val('~');
		}
		// Removes light aquares below the lightbulb
		int i = r + 1;
		while ((grid[i][c].get_val() == '*' || grid[i][c].get_val() == '~') && i < 5) {
			grid[i][c].decrease_light_count();
			if (grid[i][c].get_val() == '~' && grid[i][c].get_light_count() == 0) {
				grid[i][c].change_val(' ');
			}
			++i;
		}
		// Removes light from squares above the lightbulb
		i = r - 1;
		while ((grid[i][c].get_val() == '*' || grid[i][c].get_val() == '~') && i >= 0) {
			grid[i][c].decrease_light_count();
			if (grid[i][c].get_val() == '~' && grid[i][c].get_light_count() == 0) {
				grid[i][c].change_val(' ');
			}
			--i;
		}
		// Lights up squares to the right of the lightbulb
		int j = c + 1;
		while ((grid[r][j].get_val() == '*' || grid[r][j].get_val() == '~') && j < 5) {
			grid[r][j].decrease_light_count();
			if (grid[r][j].get_val() == '~' && grid[r][j].get_light_count() == 0) {
				grid[r][j].change_val(' ');
			}
			++j;
		}
		// Lights up squares to the left of the lightbulb
		j = c - 1;
		while ((grid[r][j].get_val() == '*' || grid[r][j].get_val() == '~') && j >= 0) {
			grid[r][j].decrease_light_count();
			if (grid[r][j].get_val() == '~' && grid[r][j].get_light_count() == 0) {
				grid[r][j].change_val(' ');
			}
			--j;
		}
	
	}
	else {
		// Displays a * to indicate a lightbulb
		grid[r][c].change_val('*');

		// Lights up squares below the lightbulb
		int i = r+1;
		while ((grid[i][c].get_val() == ' ' || grid[i][c].get_val() == '*' || grid[i][c].get_val() == '~') && i < 5) {
			if (grid[i][c].get_val() == ' ') {
				grid[i][c].change_val('~');
			}
			grid[i][c].increase_light_count();
			++i;
		}
		// Lights up squares above the lightbulb
		i = r-1;
		while ((grid[i][c].get_val() == ' ' || grid[i][c].get_val() == '*' || grid[i][c].get_val() == '~') && i >= 0) {
			if (grid[i][c].get_val() == ' ') {
				grid[i][c].change_val('~');
			}
			grid[i][c].increase_light_count();
			--i;
		}
		// Lights up squares to the right of the lightbulb
		int j = c+1;
		while ((grid[r][j].get_val() == ' ' || grid[r][j].get_val() == '*' || grid[r][j].get_val() == '~') && j < 5) {
			if (grid[r][j].get_val() == ' ') {
				grid[r][j].change_val('~');
			}
			grid[r][j].increase_light_count();
			++j;
		}
		// Lights up squares to the left of the lightbulb
		j = c-1;
		while ((grid[r][j].get_val() == ' ' || grid[r][j].get_val() == '*' || grid[r][j].get_val() == '~') && j >= 0) {
			if (grid[r][j].get_val() == ' ') {
				grid[r][j].change_val('~');
			}
			grid[r][j].increase_light_count();
			--j;
		}
	}
}

void check_answer(Box grid[5][5]) {
	// Goes through all boxes and checks for possible errors
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			char val = grid[i][j].get_val();
			int lc = grid[i][j].get_light_count();
			// Makes sure all squares are lit
			if (val == ' ') {
				cout << "Answer incorrect. Better luck next time!";
				return;
			}
			// Makes sure no lightbulb lights another lightbulb
			if (val == '*' && lc != 0) {
				cout << "Answer incorrect. Better luck next time!";
			}
			// If a box has a number, we check that the proper number of lightbulbs are adjacent to it
			if (val != '~' && val != '*' && val != '#') {
				// Count the number of *'s adjacent to the box
				int count = 0;
				if (i < 4) {
					if (grid[i + 1][j].get_val() == '*') {
						++count;
					}
				}
				if (i > 0) {
					if (grid[i - 1][j].get_val() == '*') {
						++count;
					}
				}
				if (j < 4) {
					if (grid[i][j + 1].get_val() == '*') {
						++count;
					}
				}
				if (j > 0) {
					if (grid[i][j - 1].get_val() == '*') {
						++count;
					}
				}
				if (count != (val - '0')) {
					cout << "Answer incorrect. Better luck next time!";
					return;
				}
			}
		}
	}

	//If we made it to this point, there are no mistakes!
	cout << "Congratulations! You have successfully completed this board!";
	return;
}

// Define all member functions of box class

Box::Box() {
	tap_count = 0;
	light_count = 0;
	val = ' ';
};

char* Box::get_top() {
	return top;
}

char* Box::get_middle() {
	return middle;
}

char* Box::get_bottom() {
	return bottom;
}

int Box::get_tap_count() {
	return tap_count;
}

char Box::get_val() {
	return val;
}

int Box::get_light_count() {
	return light_count;
}

void Box::increase_tap_count() {
	++tap_count;
}

void Box::increase_light_count() {
	++light_count;
}

void Box::decrease_light_count() {
	if (light_count != 0) {
		--light_count;
	}
}


void Box::change_val(char c) {
	middle[2] = c;
	val = c;
}