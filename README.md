# Lighten Up #

For my final project for PIC 10C, I decided to make a logic game. The game consists of 5 different boards the player can choose from, each of which need to be solved following certain rules:  
1. The grid represents a room where squares marked with a #,0,1,2,3,4 are walls. Your goal is to light up the entire room.  
2. Lightbulbs lite all unblocked squares horizontaly and vertically  
3. A lightbult cannot light another lightbulb  
4. Walls block light  
5. Walls with a number tell you how many lightbulbs are horizontally and vertically adjacent to it  

### Sample Gameplay ###

Please note: Formatting of the board does not line up well with the readme, please view raw to get the proper layout.

Welcome to Lighten Up!  
The grid represents a room where squares marked with a #,0,1,2,3,4 are walls.  
Your goal is to light up the entire room following these rules:  
1. Lightbulbs lite all unblocked squares horizontaly and vertically  
2. A lightbult cannot light another lightbulb  
3. Walls block light  
4. Walls with a number tell you how many lightbulbs are horizontally and vertically adjacent to it.  
A wall with a # can have any number of lightbulbs adjacent to it.  
If you "tap" a box once, a lightbulb will be placed there. If you "tap" it again, the lighbulb will be removed  

You can choose from 5 boards! Please enter a number between 1 and 5 to choose your board: 1  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││   ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││   ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││   ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││   ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││   ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  

To tap a box, enter two numbers separated by a space to indicate the coordiates of your chosen square. (The coordinates operate like those of a matrix, starting at 0)  
Which box would you like to tap? (Enter "5 5" to submit your final answer)  
1 1  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││ ~ ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││ ~ ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
3 0  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│   ││ ~ ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
0 0  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
2 2  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││ * ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││ ~ ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
2 2  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││   ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││   ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  
  

Which box would you like to tap? (Enter "5 5" to submit your final answer)  
1 3  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││ ~ ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││ * ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││ ~ ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
4 3  
Cannot place a lightbulb on a wall!  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││ ~ ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││ * ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││   ││ ~ ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││   ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  
  

Which box would you like to tap? (Enter "5 5" to submit your final answer)  
4 2  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││ ~ ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││ * ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││ ~ ││ ~ ││   │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││ * ││ 2 ││   │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
4 4  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ # ││ ~ ││ # │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ * ││ 2 ││ * ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ 1 ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ * ││ ~ ││ ~ ││ ~ ││ ~ │  
└───┘└───┘└───┘└───┘└───┘  
┌───┐┌───┐┌───┐┌───┐┌───┐  
│ ~ ││ ~ ││ * ││ 2 ││ * │  
└───┘└───┘└───┘└───┘└───┘  


Which box would you like to tap? (Enter "5 5" to submit your final answer)  
5 5  
Congratulations! You have successfully completed this board!  

### Files Included ###

* One header file: Lighten_Up.h
* One cpp file: Lighten_Up.cpp
